const express = require('express');
const app = express();

app.use(express.json());

const students = [

    {id: 1, name: 'jorge', age: 20, enroll: true},
    {id: 2, name: 'jesus', age: 20, enroll: false},
    {id: 3, name: 'mario', age: 20, enroll: false},
    
];

app.get('/', (req, res) => {
    res.send('Node JS api');
} );

app.get('/api/students', (req, res) => {
    res.send(students);
});


app.get('/api/students/:id', (req, res)=> {
    const student = students.find(c => c.id === parseInt(req.params.id));
    if (!student) return res.status(404).send('Estudiante no encontrado');
    else res.send(student);
});

const port = process.env.port || 80;
app.listen(port, () => console.log(`Escuha el puerto ${port}...`));